Firmware for RSB S2:
 * http://support.ts.fujitsu.com/Download/ShowDescription.asp?SoftwareGUID=6DB401B3-B0BD-43CB-A3AA-6278B23FBCB0
 * http://support.ts.fujitsu.com/Download/ShowDescription.asp?SoftwareGUID=4EC6401E-CA1A-47D8-A178-C75D91665CE2
Use the first link for a patchable firmware, the second is just an "update"
firmware which will not touch user settings and rights...

Documentation:
 * http://manuals.fujitsu-siemens.com/open.php?ID=4384
 * http://cp.literature.agilent.com/litweb/pdf/5988-8687EN.pdf
 * http://cp.literature.agilent.com/litweb/pdf/5988-8686EN.pdf

Software:
 * http://download.ts.fujitsu.com/prim_supportcd/start.html
 * http://download.ts.fujitsu.com/prim_supportcd/SVSSoftware/Software/Deployment/PRIMERGY%20Scripting/PRIMERGY%20Scripting%20Toolkit.msi
   (DOS utilities (spman.exe))

Buster JRE:
 * https://upload.glanzmann.de/oracle-java8-jre_8u241_amd64.deb

How to flash:
 * Download the firmware from:
   http://support.ts.fujitsu.com/Download/ShowDescription.asp?SoftwareGUID=6DB401B3-B0BD-43CB-A3AA-6278B23FBCB0
 * Extract the .exe file with unzip
 * Place RSB_S2.bin, RSB_S2.001 and RSB_S2_SINGLE.bin in the tftpboot
   directory. Make sure that all three files are readable!
 * Initiate the firmware-upgrade from the web interface:
   - When upgrading from 6.x, flash the RSB_S2.bin file, which
     will automatically load the RSB_S2.001 file in the process
   - When upgrading from 7.x, flash the RSB_S2_SINGLE.bin
   WARNING: NEVER FLASH THE .001 FILES DIRECTLY, THIS WILL BRICK YOUR BOARD!
 * Modify RSB_S2_SINGLE.bin with 'firmware' and reflash ;-)
   (Don't try to modify the other files)

Linux notes:
 * Disable ipmi_si:
   echo "blacklist ipmi_si" >/etc/modprobe.d/rsbs2.conf
   If you don't do this, the bootup process will take a long time but finally
   succeed.
 * Install required perl packages:
   apt-get install libwww-perl libxml-simple-perl libcrypt-ssleay-perl

Session timeouts:
 * Sessions on the RSBS2 card have a very long timeout of 600 minutes.
   To change this timeout to (for example) 120 minutes, change the
   SESSION_MANAGER_SESSION_TIMEOUT to 0x78:
   $ ./rsbs2.pl -s SESSION_MANAGER_SESSION_TIMEOUT=0x78 name_of_board

Pinout:
  +---------------+---------------+--------------------------------------+---+-
  |               | 2 4 6 8 A C E |                                      | 1 |
  |               | 1 3 5 7 9 B D |                                      | 2 |
  |      +-----+  +------   ------+               +---------+ +-----+    +---+
  |      | 1 2 |             CONN2                |  IPMB4  | |IPMB3|     J1
  |      | 3 4 |                                  +---------+ +-----+
  +        5 6 |                                  +---------+
 USB     | 7 8 |                                  | 1   2 3 |
         | 9 A |                                  +---------+
  +      +-----+                                       CONN3
  |       CONN1
  |
  .
  .
  .
  +                             +---+
 PWR                            | 1 |
                                | 2 |J2
  +                             +---+
  |   +-------------+            +-+
  |   |             |            | |
  +---+             +------------+ +-------

CONN1:          CONN2:              CONN3:          J1: Repair mode enable
 1)              1) ATX-PWR +*       1)             J2: Standby power enable
 2)              2) ATX-PWR GND*     2)
 3) USB D-       3) ATX-PWR +*       3)
 4)              4) ATX-PWR GND*
 5) USB D+       5) ATX-RESET +*
 6)              6) ATX-RESET GND*
 7) USB GND      7) ATX-RESET +*
 8)              8) ATX-RESET GND*
 9)              9) PWR-SW
 A)              A) PWR-SW
                 B) RESET-SW
                 C) RESET-SW
                 D)
                 E)

*) use to connect to the front panel button and pinheader on the mainboard,
   beware of the polarity!

Supermicro exception list
=========================

vim /home/sithglan/.java/./deployment/security/exception.sites

https://esx-01-mgmt.v101.tuvl.de
https://esx-02-mgmt.v101.tuvl.de
