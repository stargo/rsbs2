#define LZ_MAGIC 0x6110beef

uint8_t *extract_lz_file(uint8_t *inbuf, uint32_t *outlen , uint8_t check_crc);
uint8_t *compress_lz(uint8_t *inbuf, int32_t inlen, int32_t *outlen);
