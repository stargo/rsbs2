struct file_entry {
	char *name;
	uint8_t *start;
	int32_t length;
	uint8_t unknown;
};

struct file_entry* get_next_file(uint8_t *fw, int32_t len);
void extract_files(uint8_t *fw, int32_t len);
void replace_add_file(uint8_t *fw, int32_t len, char *fwname, char *lname);
void list_files(uint8_t *fw, int32_t len);
void write_file(char *fname, uint8_t *buf, int32_t len);
char *extracted_file(char *fname);
