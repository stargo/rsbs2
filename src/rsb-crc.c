#include <stdio.h>
#include <stdint.h>

#define POLY 0x04c11db7

uint32_t rsb_crc(uint32_t r11_crc, uint8_t *r10_buf, uint32_t r14_len) {
	uint32_t r6_pos = 0;
	uint32_t r3_data;
	int32_t r5_bit;

	while (r6_pos < r14_len) {
		r3_data = (*(r6_pos+r10_buf)) << 24;
		r11_crc = r11_crc ^ r3_data;

		r5_bit = 8;
		
		do {
			r3_data = r11_crc & 0x80000000;

			if (r3_data != 0) {
				r3_data = r11_crc << 1;
				r11_crc = r3_data ^ POLY;
			} else {
				r11_crc = r11_crc << 1;
			}
			r5_bit--;
		} while (r5_bit);

		r6_pos++;
	}

	return r11_crc;
}

uint32_t rsb_crc2(uint8_t *r0_buf, uint32_t r1_buflen, uint32_t r2_magic, uint32_t *crc_out) {
	uint32_t r4_len;
	uint32_t file_crc;

	r4_len = *(uint32_t*)(r0_buf + 0x20);

	if (*((uint32_t*)(r0_buf + 0x24)) != r2_magic)
		return 2; /* MAGIC does not match */
	
	if (r1_buflen < r4_len)
		return 3; /* image to small */
	
	*crc_out = ~rsb_crc(~0x0, r0_buf, r4_len);

	file_crc = *((uint32_t*)(r0_buf + r4_len));

	if (file_crc != *crc_out)
		return 4;

	return 0;
}
