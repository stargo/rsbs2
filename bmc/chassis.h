#define CHASSIS_ACTION_POWER_DOWN	0x00
#define CHASSIS_ACTION_POWER_UP		0x01
#define CHASSIS_ACTION_POWER_CYCLE	0x02
#define CHASSIS_ACTION_HARD_RESET	0x03
#define CHASSIS_ACTION_SOFT_SHUTDOWN	0x05

void chassis_init();
void chassis_control(unsigned char action);
