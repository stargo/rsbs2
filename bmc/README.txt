This directory includes firmware for an I2C BMC emulator running on an Atmel
AVR with TWI. This makes it possible to use the Power On/Power Off functional-
ity of the Java GUI to control the machines power.

Suggested microcontroller when building the hardware: ATmega328P

The software is currently only tested on an ATmega16, as I had that one
lying around...

Currently the following PINs act as low active connections to the motherboard:
PB0 - Power button
PB1 - Reset button

A picture of my prototypical setup can be found in proto_setup.jpg
