#include <avr/io.h>
#include <stdio.h>

#include "config.h"
#include "usart.h"

#ifdef DEBUG

#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD)

#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch! 
#endif 

int usart_put(char c, FILE *stream)
{
	if (c == '\n')
		usart_put('\r', stream);

	while(!(UCSRA & (1<<UDRE))) {}
	UDR = c;
	return 0;
}

static FILE usart_stdout = FDEV_SETUP_STREAM(usart_put, NULL, _FDEV_SETUP_WRITE);
#endif

void usart_init()
{
#ifdef DEBUG
	UCSRB |= (1<<TXEN);
	UCSRC |= (1<<URSEL)|(1 << UCSZ1)|(1 << UCSZ0);

	UBRRH = UBRR_VAL >> 8;
	UBRRL = UBRR_VAL & 0xFF;

	stdout = &usart_stdout;
#endif
}
