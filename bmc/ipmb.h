#define IPMB_APP_GET_DEVICE_ID		0x01
#define IPMB_APP_GET_WATCHDOG_TIMER	0x25

#define IPMB_CHASSIS_GET_STATUS		0x01
#define IPMB_CHASSIS_CONTROL		0x02

#define IPMB_SE_PLATFORM_EVENT		0x02

#define IPMB_STORAGE_RESERVE_SDR	0x22
#define IPMB_STORAGE_GET_SDR		0x23
#define IPMB_STORAGE_GET_SEL_INFO	0x40
#define IPMB_STORAGE_GET_SEL_ALLOCATION	0x41
#define IPMB_STORAGE_GET_SEL_ENTRY	0x43
#define IPMB_STORAGE_GET_SEL_TIME	0x48

#define IPMB_NETFN_CHASSIS		0x00
#define IPMB_NETFN_BRIDGE		0x02
#define IPMB_NETFN_SENSOR_EVENT		0x04
#define IPMB_NETFN_APP			0x06
#define IPMB_NETFN_FIRMWARE		0x08
#define IPMB_NETFN_STORAGE		0x0a

#define IPMB_CC_NORMALLY		0x00
#define IPMB_CC_BUSY			0xc0
#define IPMB_CC_INVALID			0xc1
#define IPMB_CC_ERROR			0xff

/* ipmb1010ltd.pdf page 27 (33) */
struct ipmb_req {
	uint8_t rsSA;
	uint8_t netFn;
	uint8_t rsLUN;
	uint8_t rqSA;
	uint8_t rqSEQ;
	uint8_t rqLUN;
	uint8_t cmd;
	unsigned char *data;
	uint8_t datalen;
};

struct ipmb_resp {
	uint8_t rqSA;
	uint8_t netFn;
	uint8_t rqLUN;
	uint8_t rsSA;
	uint8_t rqSEQ;
	uint8_t rsLUN;
	uint8_t cmd;
};

void decode_ipmb_pkt(unsigned char *buf, int len);
