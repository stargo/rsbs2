#include <util/twi.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "i2c.h"
#include "bmc.h"
#include "config.h"
#include "ipmb.h"

#define TWCR_ACK TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);  
#define TWCR_NACK TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(0<<TWEA)|(0<<TWSTA)|(0<<TWSTO)|(0<<TWWC);
#define TWCR_RESET TWCR = (1<<TWEN)|(1<<TWIE)|(1<<TWINT)|(1<<TWEA)|(0<<TWSTA)|(1<<TWSTO)|(0<<TWWC);  

volatile unsigned char i2c_databuf[24];
volatile uint8_t i2c_len = 0x00;
static volatile uint8_t i2c_pos = 0x00;
volatile uint8_t i2c_done = 0x00;

#define I2C_FREQ 20000UL

void i2c_init()
{
	/* SCLf = F_CPU / (16 + 2 * TWBR * 4^TWPS)
	 * TWPS is 0 =>
	 * TWBR = (F_CPU/(2 * SCL)) - 8
	 */
	TWBR = (F_CPU/(2*I2C_FREQ))-8;
	TWAR = BMC_ADDR & 0xfe;
	TWDR = 0x00;
	TWCR &= ~((1<<TWSTA) | (1<<TWSTO));
	TWCR |= ((1<<TWEA) | (1<<TWEN) | (1<<TWIE)); 
#ifdef __AVR_ATmega16__
	PORTC = 0x03;
#else
#error "Don't know how to set pullups for this chip, please add support"
#endif
}

void i2c_send(unsigned char *buf, int len)
{
	uint8_t old_TWCR = TWCR;
	uint8_t old_SREG = SREG;
	int i;

	cli();

	TWCR = ((1<<TWINT) | (1<<TWSTA) | (1<<TWEN)); /* Send start */

	while(!(TWCR & (1<<TWINT))) {}
	if (TW_STATUS != TW_START) {
#ifdef DEBUG
		printf("I2C: error sending START\n");
#endif
		goto out;
	}

	TWDR = buf[0]; /* SLA_W */
	TWCR = ((1<<TWINT) | (1<<TWEN));

	while(!(TWCR & (1<<TWINT))) {}
	if (TW_STATUS != TW_MT_SLA_ACK) {
#ifdef DEBUG
		printf("I2C: error sending SLA_W\n");
#endif
		goto out2;
	}
	
	for(i = 1; i < len; i++) {
		TWDR = buf[i]; /* Send Data */
		TWCR = ((1<<TWINT) | (1<<TWEN));

		while(!(TWCR & (1<<TWINT))) {}
		if (TW_STATUS != TW_MT_DATA_ACK) {
#ifdef DEBUG
			printf("I2C: error sending DATA byte %d\n", i);
#endif
			goto out2;
		}
	}

#ifdef DEBUG
	printf("I2C Data sent\n");
#endif

out2:
	TWCR = ((1<<TWINT) | (1<<TWEN) | (1<<TWSTO));
	while(TWCR & (1<<TWSTO)) {}

out:	
	SREG = old_SREG;
	TWCR = old_TWCR | (1<<TWINT);
}

ISR (TWI_vect, ISR_BLOCK)
{
	if (i2c_done)
		TWCR_RESET;

	switch (TW_STATUS) {
		case TW_SR_SLA_ACK:
#ifdef DEBUG
			printf("I2C: Slave 0x%02x adressed\n", TWDR);
#endif
			i2c_pos = 0x00;
			i2c_databuf[i2c_pos] = TWDR;
			i2c_pos++;
			TWCR_ACK;
			break;

		case TW_SR_DATA_ACK:
#ifdef DEBUG
			printf("I2C: Data received: 0x%02x\n", TWDR);
#endif
			if (i2c_pos >= sizeof(i2c_databuf)) {
				TWCR_RESET;
				i2c_pos = 0x00;
				break;
			}
			i2c_databuf[i2c_pos] = TWDR;
			i2c_pos++;
			TWCR_ACK;
			break;

		case TW_SR_STOP:
#ifdef DEBUG
			printf("I2C: STOP received\n");
#endif
			i2c_len = i2c_pos;
			i2c_pos = 0x00;
			i2c_done = 0x01;
			TWCR_RESET;
			break;

		case TW_NO_INFO:
#ifdef DEBUG
			printf("I2C: TW_NO_INFO received status 0x%2x\n", TW_STATUS);
#endif
			TWCR |= (1<<TWINT);
			break;

		default:
#ifdef DEBUG
			printf("I2C: Unimplemented status 0x%02x\n", TW_STATUS);
#endif
			TWCR_RESET;
			break;
	}
}
