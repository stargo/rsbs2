#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <string.h>
#include <stdio.h>
#include "usart.h"
#include "i2c.h"
#include "ipmb.h"
#include "chassis.h"
#include "bmc.h"

#undef SLEEP_MODE

int main(void)
{
	unsigned char buf[24];
	unsigned int len;

	set_sleep_mode(SLEEP_MODE_IDLE);

	chassis_init();
	usart_init();
	sei();
	i2c_init();

	while(1) {
		cli();
		while (i2c_done) {
			len = i2c_len;
			memcpy(buf, (unsigned char*)i2c_databuf, len);
			i2c_done = 0x00;
			sei();

			decode_ipmb_pkt(buf, len);

			cli();
		}

#ifdef SLEEP_MODE
		sleep_enable();
#endif
		sei();
#ifdef SLEEP_MODE
		sleep_cpu();
		sleep_disable();
#endif
	}

	return 0;
}
