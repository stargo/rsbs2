extern volatile unsigned char i2c_databuf[24];
extern volatile uint8_t i2c_len;
extern volatile uint8_t i2c_done;

void i2c_init();
void i2c_send(unsigned char *buf, int len);
