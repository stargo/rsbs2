all: firmware

firmware: src/firmware
	ln -sf $^ $@

src/firmware:
	$(MAKE) -C src firmware

clean:
	rm -f firmware
	$(MAKE) -C src clean

.PHONY: all clean src/firmware
